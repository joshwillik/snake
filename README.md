# Snake

This is just a hobby implementation of snake to teach myself Rust.

## Installation

```sh
$ git clone https://gitlab.com/joshwillik/snake
$ cd snake
$ cargo run --release
```

## Known problems

- The terminal flickers as the game runs. I'm as of yet unsure if this is because rendering the full screen per frame is too much text output for the terminal to handle. Maybe I need to implement diffing and only write the changes.
- The snake moves much faster on the Y axis than the X axis. One solution is to move to a real pixel rendered screen. Another solution to this is to implement a render a pixel screen down to the terminal using half height box characters.
- The game start and game over sequence is pretty abrupt, need to add an intro and outro state
- The game overall is pretty boring, need to spice it up a bit