pub enum InputEvent {
    GoLeft,
    GoRight,
    GoUp,
    GoDown,
    Pause,
    Debug,
    Quit,
}

pub trait InputEvents {
    fn next(&mut self) -> Option<InputEvent>;
}
