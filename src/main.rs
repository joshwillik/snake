mod game;
mod screen;
mod inputs;
mod terminal;

use game::{GameClient, GameOptions};
use terminal::{terminal_size, TerminalInputs, TerminalScreen};

fn main() {
    let mut game = GameClient::new(GameOptions {
        size: terminal_size(),
        screen: Box::new(TerminalScreen::new()),
        input_events: Box::new(TerminalInputs::new()),
    });
    game.run();
}