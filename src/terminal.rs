use std::io::{Write, stdin, stdout, Stdout};
use std::sync::mpsc;
use std::thread;

use termion::raw::{IntoRawMode, RawTerminal};
use termion::event::Key;
use termion::input::TermRead;
use termion::{clear, cursor};

use crate::game::{GameState, GameStatus};
use crate::inputs::{InputEvents, InputEvent};
use crate::screen::GameScreen;

pub fn terminal_size() -> (u16, u16) {
    termion::terminal_size().unwrap()
}

pub struct TerminalInputs {
    events: mpsc::Receiver<Result<Key, std::io::Error>>,
}

pub struct TerminalScreen {
    term: RawTerminal<Stdout>,
}

impl TerminalScreen {
    pub fn new() -> Self {
        let stdout = stdout();
        if !termion::is_tty(&stdout) {
            panic!("You can't run a terminal game with a pipe you goob");
        }
        Self {
            term: stdout.into_raw_mode().unwrap(),
        }
    }
}

impl GameScreen for TerminalScreen {
    fn render(&mut self, state: &GameState) {
        render_terminal(&mut self.term, state);
    }
}

impl TerminalInputs {
    pub fn new() -> Self {
        let (tx, rx) = mpsc::channel();
        thread::spawn(move || {
            for k in stdin().keys() {
                tx.send(k).unwrap();
            }
        });
        Self { events: rx }
    }
}

impl InputEvents for TerminalInputs {
    fn next(&mut self) -> Option<InputEvent> {
        match self.events.try_recv() {
            Ok(k) => match k.unwrap() {
                Key::Up => Some(InputEvent::GoUp),
                Key::Down => Some(InputEvent::GoDown),
                Key::Left => Some(InputEvent::GoLeft),
                Key::Right => Some(InputEvent::GoRight),
                Key::Char('d') => Some(InputEvent::Debug),
                Key::Char('p') => Some(InputEvent::Pause),
                Key::Char('q') => Some(InputEvent::Quit),
                Key::Esc => Some(InputEvent::Quit),
                _ => None,
            },
            Err(_) => None,
        }

    }
}

fn render_terminal(term: &mut RawTerminal<Stdout>, game: &GameState) {
    write!(term, "{}{}{}", clear::All, cursor::Hide, cursor::Goto(1, 1)).unwrap();
    if game.status == GameStatus::Quit {
        write!(term, "{}\n", cursor::Show).unwrap();
        return;
    }
    if game.debug {
        write!(term, "{:?}", game).unwrap();
        term.flush().unwrap();
        return 
    }
    for x in game.field.left..game.field.right {
        write!(term, "{}-", cursor::Goto(x as u16 + 1, game.field.top as u16 + 1)).unwrap();
    }
    for x in game.field.left..game.field.right {
        write!(term, "{}-", cursor::Goto(x as u16 + 1, game.field.bottom as u16 + 1)).unwrap();
    }
    for x in game.field.top..game.field.bottom {
        write!(term, "{}|", cursor::Goto(game.field.left as u16 + 1, x as u16 + 1)).unwrap();
    }
    for x in game.field.top..game.field.bottom {
        write!(term, "{}|", cursor::Goto(game.field.right as u16 + 1, x as u16 + 1)).unwrap();
    }
    for point in &game.pills {
        write!(term, "{}o", cursor::Goto(point.0 as u16 + 1, point.1 as u16 + 1)).unwrap();
    }
    for point in &game.snake.body {
        write!(term, "{}#", cursor::Goto(point.0 as u16 + 1, point.1 as u16 + 1)).unwrap();
    }
    term.flush().unwrap();
}