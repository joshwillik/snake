use crate::game::{GameState};

pub trait GameScreen {
    fn render(&mut self, state: &GameState);
}