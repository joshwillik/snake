use std::collections::HashSet;
use std::mem;
use std::thread;
use std::time;

use rand::Rng;

use crate::inputs::{InputEvents, InputEvent as Event};
use crate::screen::{GameScreen};

pub struct GameOptions {
    pub size: (u16, u16),
    pub input_events: Box<dyn InputEvents>,
    pub screen: Box<dyn GameScreen>,
}

pub struct GameClient {
    pub state: GameState,
    pub screen: Box<dyn GameScreen>,
    pub input_events: Box<dyn InputEvents>,
}

#[derive(Debug)]
pub struct GameState {
    pub status: GameStatus,
    pub paused: bool,
    pub debug: bool,
    // pub score: u32,
    pub snake: Snake,
    pub pills: Vec<Pill>,
    pub pending_pills: u32,
    pub field: Field,
}

impl GameClient {
    pub fn new(options: GameOptions) -> Self {
        GameClient {
            state: GameState {
                paused: false,
                debug: false,
                status: GameStatus::Running,
                snake: Snake::new(),
                pills: Vec::new(),
                pending_pills: 3,
                // score: 0,
                field: Field::from_size(options.size),
            },
            screen: options.screen,
            input_events: options.input_events,
        }
    }

    fn tick(&mut self) {
        process_inputs(self);
        if self.state.paused {
            return;
        }
        move_snake(&mut self.state);
        eat_pills(&mut self.state);
        grow_snake(&mut self.state);
        add_pills(&mut self.state);
        detect_death(&mut self.state);
    }

    fn render(&mut self) {
        self.screen.render(&self.state);
    }

    pub fn run(&mut self) {
        while self.state.status!=GameStatus::Quit {
            self.tick();
            self.render();
            thread::sleep(time::Duration::from_millis(50));
        }
    }
}

pub type Pill = Point;

#[derive(Debug)]
pub struct Snake {
    pub direction: Direction,
    pub body: Vec<Point>,
    pub digesting: Vec<Point>,
}

impl Snake {
    fn new() -> Self {
        Snake {
            direction: Direction::Right,
            body: vec![
                Point(10, 10),
                Point(9, 10),
            ],
            digesting: Vec::new(),
        }
    }
    fn head_position(&self) -> &Point {
        &self.body[0]
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Direction {
    Right,
    Left,
    Up,
    Down,
}

impl Direction {
    fn opposite(&self) -> Self {
        match self {
            Self::Right => Self::Left,
            Self::Left => Self::Right,
            Self::Up => Self::Down,
            Self::Down => Self::Up,
        }
    }
}

#[derive(PartialEq, Eq, Debug)]
pub enum GameStatus {
    // Start,
    Running,
    Quit,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub struct Point(pub i16, pub i16);

#[derive(Debug)]
pub struct Field {
    pub top: i16,
    pub bottom: i16,
    pub left: i16,
    pub right: i16,
    // pub height: u16,
    // pub width: u16,
}

impl Field {
    fn from_size((x, y): (u16, u16)) -> Self {
        Self {
            top: 0,
            bottom: y as i16,
            left: 0,
            right: x as i16,
            // width: x,
            // height: y,
        }
    }
    fn includes(&self, point: &Point) -> bool {
        return !(point.0 < self.left || point.0 > self.right ||
            point.1 < self.top || point.1 > self.bottom);
    }
}

fn process_inputs(game: &mut GameClient) {
    let state = &mut game.state;
    loop {
        match game.input_events.next() {
            Some(event) => {
                let new_direction = match event {
                    Event::GoUp => Some(Direction::Up),
                    Event::GoDown => Some(Direction::Down),
                    Event::GoLeft => Some(Direction::Left),
                    Event::GoRight => Some(Direction::Right),
                    _ => None,
                };
                if let Some(next_direction) = new_direction {
                    if next_direction.opposite()!=state.snake.direction {
                        state.snake.direction = next_direction;
                    }
                }
                match event {
                    Event::Debug => state.debug = !state.debug,
                    Event::Pause => state.paused = !state.paused,
                    Event::Quit => state.status = GameStatus::Quit,
                    _ => (),
                };
            },
            None => break,
        }
    }
}

fn move_snake(game: &mut GameState) {
    let Point(mut x, mut y) = game.snake.head_position();
    match game.snake.direction {
        Direction::Left => x -= 1,
        Direction::Right => x += 1,
        Direction::Up => y -= 1,
        Direction::Down => y += 1,
    };
    game.snake.body.iter_mut().for_each(|point| {
        mem::swap(&mut x, &mut point.0);
        mem::swap(&mut y, &mut point.1);
    })
}

fn eat_pills(game: &mut GameState) {
    let head = game.snake.head_position();
    let (eaten, remaining) = game.pills.clone().into_iter().partition(|pill| pill==head);
    game.pills = remaining;
    for pill in eaten {
        game.snake.digesting.push(pill);
        game.pending_pills += 1;
    }
}

fn grow_snake(game: &mut GameState) {
    let (growths, digesting) = game.snake.digesting.clone().into_iter().partition(|pill| {
        for segment in &game.snake.body {
            if *segment==*pill {
                return false;
            }
        }
        return true;
    });
    for growth in growths {
        game.snake.body.push(growth);
    }
    game.snake.digesting = digesting;
}

fn add_pills(state: &mut GameState) {
    if !state.pending_pills==0 {
        return;
    }
    for _ in 0..state.pending_pills {
        let x = rand::thread_rng().gen_range(state.field.left..state.field.right-1);
        let y = rand::thread_rng().gen_range(state.field.top..state.field.bottom-1);
        state.pills.push(Point(x, y));
    }
    state.pending_pills = 0;
}

fn detect_death(state: &mut GameState) {
    if !state.field.includes(state.snake.head_position()) {
        state.status = GameStatus::Quit;
    }
    let mut seen = HashSet::new();
    for point in &state.snake.body {
        if !seen.insert(point) {
            state.status = GameStatus::Quit;
        }
    }
}
